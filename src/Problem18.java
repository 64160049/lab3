import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        int starType;
        int n;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please select star type [1-4,5 is Exit]:");
        starType = sc.nextInt();
        if (starType == 1) {
            System.out.print("Please input n: ");
            n = sc.nextInt();
            for (int i = 1; i <= n; i++) {
                for (int j = 0; j < i; j++) {
                    System.out.print("*");
                }
                System.out.println();
            }
        }
        if (starType == 2) {
            System.out.print("Please input n: ");
            n = sc.nextInt();
            for (int i = n; i >= 1; i--) {
                for (int j = 0; j < i; j++) {
                    System.out.print("*");
                }
                System.out.println();
            }
        }
        if (starType == 5) {
            System.out.println("Bye bye!!!");
            
        }
        if (starType > 5) {
            System.out.print("Error: Please input number between 1-5");
        }
    }
}
